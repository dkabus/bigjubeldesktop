# BigJubel Desktop Client

> Calculate jubilees that you might otherwise miss,
> for instance the 7777-day jubilee of your birth!

Run [BigJubel](https://gitlab.com/dkabus/bigjubel) as a [`pywebview`](https://pywebview.flowrl.com/) app.
By default it sets up a simple single-user instance perfect for individual usage.

It builds upon the command line utility [`bigjubel`](https://gitlab.com/dkabus/bigjubelcli)
and the web server and interface [`bigjubelweb`](https://gitlab.com/dkabus/bigjubelweb).

The source code of `bigjubeldesktop` is open and can be found here:
<https://gitlab.com/dkabus/bigjubeldesktop>

For help and usage, check out the manuals for BigJubel:

- [`man bigjubelweb`](https://dkabus.gitlab.io/bigjubelweb/man.html)
- [`man bigjubel`](https://dkabus.gitlab.io/bigjubel/man.html)
- [`man bigjubeldesktop`](https://dkabus.gitlab.io/bigjubeldesktop/man.html)

## Installation
On Arch Linux based distibutions, you can install the [AUR package `bigjubeldesktop-git`](https://aur.archlinux.org/packages/bigjubeldesktop-git/).

Alternatively, follow these steps:

Install the dependencies:

- BigJubel Web [`bigjubelweb`](https://gitlab.com/dkabus/bigjubelweb) and its dependencies
- `pywebview`

Then:
```
make
sudo make install
```
