logosizes := 16 24 32 48 64 96 128 256 512
logos := $(shell echo $(logosizes) | tr ' ' '\n' | sed 's/^/logo\./;s/$$/\.png/' | tr '\n' ' ')

all: $(logos)

logos: $(logos)

logo.%.png: logo.png
	export i=$(shell echo $@ | cut -d. -f2);\
	convert logo.png -filter point -resize $${i}x$${i} logo.$${i}.png

clean:
	rm -f logo.*.png

install: $(logos)
	install -Dm755 "bigjubeldesktop" -t "$(DESTDIR)/usr/bin/"
	install -Dm644 "bigjubel.desktop" -t "$(DESTDIR)/usr/share/applications/"
	install -Dm644 "LICENSE" -t "$(DESTDIR)/usr/share/licenses/bigjubeldesktop/"
	mkdir -p "$(DESTDIR)/usr/share/man/man1"
	sed "s/VERSION/$(shell ./bigjubeldesktop -v)/g" < bigjubeldesktop.1 \
		> "$(DESTDIR)/usr/share/man/man1/bigjubeldesktop.1"
	for i in $(logosizes); do \
		install -Dm644 logo.$${i}.png \
			"$(DESTDIR)/usr/share/icons/hicolor/$${i}x$${i}/apps/bigjubeldesktop.png"; \
	done

uninstall:
	rm -f "$(DESTDIR)/usr/local/bin/bigjubeldesktop"
	rm -f "$(DESTDIR)/usr/share/applications/bigjubel.desktop"
	rm -f "$(DESTDIR)/usr/share/licenses/bigjubeldesktop/LICENSE"
	rmdir "$(DESTDIR)/usr/share/licenses/bigjubeldesktop"
	rm -f "$(DESTDIR)/usr/share/man/man1/bigjubeldesktop.1"
	for i in $(logosizes); do \
		rm -f "$(DESTDIR)/usr/share/icons/hicolor/$${i}x$${i}/apps/bigjubeldesktop.png"; \
	done

.PHONY: all logos clean install uninstall
